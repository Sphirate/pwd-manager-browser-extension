# PWD Manager (Browser extension)

Simple test browser extension to check for password complexity on the fly

## Install

1. Clone this repository to local machine
2. In terminal/console navigate to created directory
3. Install NPM dependencies. Run `npm install` in terminal/console
4. Run build command `npm run build` in terminal/console

After that steps you'll have 'extension' directory. In that directory will be code of extension

To install this extension in your browser you need to add it in browser

1. Open Google Chrome browser
2. Open Extensions tab in it
3. In that tab select 'Developer mode' checkbox
4. Click 'Install unpacked extension' and select 'extension' directory in local repository directory
