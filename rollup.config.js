const indexConfig = {
    input: './src/index.js',
    output: {
        file: './extension/js/index.js',
        format: 'es',
    },
};

const contentConfig = {
    input: './src/content/index.js',
    output: {
        file: './extension/js/content.js',
        format: 'es',
    },
};

const backgroundConfig = {
    input: './src/background.js',
    output: {
        file: './extension/js/background.js',
        format: 'es',
    },
};

export default [indexConfig, contentConfig, backgroundConfig];
