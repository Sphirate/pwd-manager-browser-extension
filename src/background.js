const getShowHint = () => JSON.parse(localStorage.getItem('PWD_Manager-show-hint') || 'true');
const saveShowHint = value => localStorage.setItem('PWD_Manager-show-hint', JSON.stringify(value));

const forEachTab = fn => chrome.tabs.query({}, tabs => tabs.forEach(fn));

let showHint = getShowHint();

const handlers = {
    CHANGE_SHOW_HINT: (payload) => {
        showHint = !!payload;
        saveShowHint(showHint);
        forEachTab(({ id }) => chrome.tabs.sendMessage(id, { type: 'CHANGE_SHOW_HINT', payload: showHint }));
        return showHint;
    },
    GET_SHOW_HINT: () => showHint,
};

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    const { type, payload } = message || {};
    if (sender.id && sender.id === chrome.runtime.id && handlers[type]) {
        sendResponse(handlers[type](payload));
    }
});
