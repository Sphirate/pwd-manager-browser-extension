import calculateStrength from './strength-calculator';

const wrapper = document.createElement('div');
wrapper.classList.add('PWD_Manager--wrapper');
document.body.appendChild(wrapper);

export default (input) => {
    let strength = null;

    // Hint element
    const hint = document.createElement('div');
    hint.classList.add('PWD_Manager--hint');

    // Eye element
    const eye = document.createElement('div');
    eye.classList.add('PWD_Manager--eye');
    hint.appendChild(eye);

    // Tooltip element
    const tooltip = document.createElement('div');
    tooltip.classList.add('PWD_Manager--tooltip');
    hint.appendChild(tooltip);

    wrapper.appendChild(hint);

    const updatePosition = () => {
        const inputRect = input.getBoundingClientRect();

        hint.style.top = `${inputRect.top}px`;
        hint.style.left = `${inputRect.right}px`;
        hint.style.height = `${inputRect.height}px`;
    };

    updatePosition();

    const handleInputChange = () => {
        const currentStrength = calculateStrength(input.value);

        if (strength !== currentStrength) {
            strength = currentStrength;
            hint.dataset.pwdStrength = strength;
        }
    };

    input.addEventListener('input', handleInputChange, false);
    handleInputChange();

    const handleEyeClick = () => {
        hint.classList.toggle('PWD_Manager--hint-disabled');
        input.focus();
    };
    eye.addEventListener('click', handleEyeClick, false);

    const handleInputFocus = () => hint.classList.remove('PWD_Manager--hint-hidden');
    const handleInputBlur = () => hint.classList.add('PWD_Manager--hint-hidden');

    input.addEventListener('focus', handleInputFocus, false);
    input.addEventListener('blur', handleInputBlur, false);
    handleInputBlur();

    return {
        update: updatePosition,
        cancel: () => {
            eye.removeEventListener('click', handleEyeClick, false);
            input.removeEventListener('input', handleInputChange, false);
            input.removeEventListener('focus', handleInputFocus, false);
            input.removeEventListener('blur', handleInputBlur, false);
            wrapper.removeChild(hint);
        },
    };
};
