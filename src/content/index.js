import onShowHintChange from './show-hint-state';
import mountStateChangeListener from './mutations';
import { clearAll, handleInputMount, handleInputUnmount } from './mount-handlers';

let cancel;
onShowHintChange((showHint) => {
    if (showHint && !cancel) {
        cancel = mountStateChangeListener(handleInputMount, handleInputUnmount);
    } else if (cancel && typeof cancel === 'function') {
        clearAll();
        cancel();
        cancel = undefined;
    }
});
