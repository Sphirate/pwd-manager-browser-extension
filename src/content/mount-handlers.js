import addHint from './hint';


const inputs = new Map();
const handleWindowResize = () => Array.from(inputs.values()).forEach(({ update }) => update());

export const handleInputMount = (input) => {
    if (inputs.has(input)) {
        inputs.get(input).update();
        return;
    }
    if (inputs.size === 0) {
        window.addEventListener('resize', handleWindowResize, false);
    }
    const removeHandler = addHint(input);
    inputs.set(input, removeHandler);
};

export const handleInputUnmount = (input) => {
    if (inputs.has(input)) {
        const destroy = inputs.get(input).cancel;
        if (typeof destroy === 'function') {
            destroy();
        }
        inputs.delete(input);
        if (inputs.size === 0) {
            window.removeEventListener('resize', handleWindowResize, false);
        }
    }
};

export const clearAll = () => Array.from(inputs.keys()).forEach(handleInputUnmount);
