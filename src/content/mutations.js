/**
 * Check if `rootNode` is password input or search for such inputs in subtree
 * and call predicate for every found password input
 * @param {Node} rootNode Root node to check for password inputs
 * @param {function(HTMLInputElement): void} handler Predecate to execute on every nested password inputs
 * @return {void}
 */
const forEachChildPwdInput = (rootNode, handler) => {
    if (!(rootNode instanceof HTMLElement)) {
        return;
    }
    if (rootNode instanceof HTMLInputElement && rootNode.type === 'password') {
        handler(rootNode);
        return;
    }
    Array.from(rootNode.querySelectorAll('input[type="password"]'))
        .forEach(input => handler(input));
};

/**
 * Start watching DOM changes for `rootNode` children using `MutationObserver`
 * @param {Node} rootNode Root node to observe
 * @param {function(HTMLInputElement): void} onMount Predicate to execute on every inserted password input
 * @param {function(HTMLInputElement): void} onUnmount Predecate to execute on every removed password input
 * @returns {function(): void} Stop watching function
 */
const observerMountHandler = (rootNode, onMount, onUnmount) => {
    const observer = new MutationObserver((mutations) => {
        const { added, removed } = mutations.reduce((acc, next) => ({
            added: [...acc.added, ...Array.from(next.addedNodes)],
            removed: [...acc.removed, ...Array.from(next.removedNodes)],
        }), { added: [], removed: [] });

        added.forEach(node => forEachChildPwdInput(node, onMount));
        removed.forEach(node => forEachChildPwdInput(node, onUnmount));
    });

    observer.observe(rootNode, { childList: true, subtree: true });

    return observer.disconnect.bind(observer);
};

/**
 * Start watching DOM changes for `rootNode` children using DOM event listeners
 * @param {Node} rootNode Root node to observe
 * @param {function(HTMLInputElement): void} onMount Predicate to execute on every inserted password input
 * @param {function(HTMLInputElement): void} onUnmount Predecate to execute on every removed password input
 * @returns {function():void} Stop watching callback
 */
const eventMountHandler = (rootNode, onMount, onUnmount) => {
    const handleInsert = event => forEachChildPwdInput(event.target, onMount);
    const handleRemove = event => forEachChildPwdInput(event.target, onUnmount);

    rootNode.addEventListener('DOMNodeInserted', handleInsert, false);
    rootNode.addEventListener('DOMNodeRemoved', handleRemove, false);

    return () => {
        rootNode.removeEventListener('DOMNodeInserted', handleInsert);
        rootNode.removeEventListener('DOMNodeRemoved', handleRemove);
    };
};

const mountStateChangeListenerFn = window.MutationObserver ? observerMountHandler : eventMountHandler;

export default (onMount, onUnmount) => {
    const rootNode = document.body;
    forEachChildPwdInput(rootNode, onMount);
    return mountStateChangeListenerFn(rootNode, onMount, onUnmount);
};
