const listeners = [];

let globalShowHint = null;
const updateGlobalShowHint = (state) => {
    if (globalShowHint !== !!state) {
        globalShowHint = !!state;
        listeners.forEach(cb => cb(globalShowHint));
    }
};

chrome.runtime.sendMessage({ type: 'GET_SHOW_HINT' }, updateGlobalShowHint);

chrome.runtime.onMessage.addListener((message, sender) => {
    const { type, payload } = message || {};
    if (sender.id && sender.id === chrome.runtime.id && type === 'CHANGE_SHOW_HINT') {
        updateGlobalShowHint(payload);
    }
});

export default (cb) => {
    listeners.push(cb);
    return () => {
        const index = listeners.indexOf(cb);
        if (index > -1) {
            listeners.splice(index, 1);
        }
    };
};
