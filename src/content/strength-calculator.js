const buildRule = (fn, trueScore, falseScore = 0) => (pwd) => {
    if (fn(pwd)) {
        return trueScore;
    }
    return falseScore;
};

const regExpRuleFn = regexp => pwd => regexp.test(pwd);

const rules = [
    pwd => Math.min((pwd.length - 8) * 2, 20),
    buildRule(regExpRuleFn(/[a-z]/), 10),
    buildRule(regExpRuleFn(/[A-Z]/), 20),
    buildRule(regExpRuleFn(/\d/), 20),
    buildRule(regExpRuleFn(/[!@#$%^&*()_+=[\]{}'"|\\/?.,`~\s]/i), 20),
    buildRule(regExpRuleFn(/([\s\S])\1{2,}/), -20),
    buildRule(pwd => !pwd || pwd.length === 0, -100),
];

const calculateStrength = pwd => rules.reduce((acc, getScore) => acc + getScore(pwd), 0);

const clamp = (max, min = 0) => value => Math.max(min, Math.min(value, max));
const partialLerp = (maxOld, maxNew) => value => (value / maxOld) * maxNew;

const transform = [
    Math.floor,
    partialLerp(90, 5),
    clamp(90, 0),
    calculateStrength,
];

const transformFn = pwd => transform.reduceRight((acc, next) => next(acc), pwd);

const memoize = (fn) => {
    const cache = {};

    return (pwd) => {
        if (!Object.prototype.hasOwnProperty.call(cache, pwd)) {
            cache[pwd] = fn(pwd);
        }
        return cache[pwd];
    };
};
export default memoize(transformFn);
