const input = document.getElementById('show-hint-input');

const setInputCheckedState = (state) => {
    input.checked = state;
};

setInputCheckedState(true);

chrome.runtime.sendMessage({ type: 'GET_SHOW_HINT' }, setInputCheckedState);

input.addEventListener('click', (event) => {
    chrome.runtime.sendMessage({ type: 'CHANGE_SHOW_HINT', payload: event.target.checked }, setInputCheckedState);
}, false);
